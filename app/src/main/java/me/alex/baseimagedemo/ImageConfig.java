package me.alex.baseimagedemo;

import me.alex.aleximage.BaseImageConfig;

/**
 * ================================================
 * Description:
 * <p>
 * Created by Alex on 2020/12/4 0004
 * <p>
 * 页面内容介绍:
 * 继承自 {@link BaseImageConfig} 实现了基本功能需求字段
 * 如需添加一个自定义功能
 * 自行实现构造方法并且在自定义的{@link me.alex.aleximage.BaseImageLoaderStrategy}中重写loadImage接口实现方法
 * 参照{@link me.alex.aleximage.SImage}
 * <p>
 * ================================================
 */
public class ImageConfig extends BaseImageConfig {

    private boolean isVisibility;

    public void setVisibility(boolean visibility) {
        isVisibility = visibility;
    }

    public ImageConfig() {

    }

    public ImageConfig(Builder builder) {
        super(builder);
    }

}
