package me.alex.baseimagedemo;

import android.app.Application;

import me.alex.aleximage.SImageConfig;

/**
 * ================================================
 * Description:
 * <p>
 * Created by Alex on 2020/12/2 0002
 * <p>
 * 页面内容介绍:
 * <p>
 * ================================================
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SImageConfig.getInstance().setBitmapPoolSize(50).setMemoryCacheSize(30);
    }
}
