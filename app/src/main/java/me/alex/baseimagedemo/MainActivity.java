package me.alex.baseimagedemo;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.target.Target;

import me.alex.aleximage.BaseImageConfig;
import me.alex.aleximage.SImage;
import me.alex.aleximage.entity.ImageResult;
import me.alex.aleximage.listener.OnBitmapResult;
import me.alex.aleximage.listener.OnDrawableResult;
import me.alex.aleximage.listener.OnFileResult;
import me.alex.aleximage.listener.OnGifResult;
import me.alex.aleximage.listener.OnLoadListener;

public class MainActivity extends AppCompatActivity {

    ImageView img1, img2, img3, img4, img5, img6, img7, img10, img11, img12, img13, img14;
    SImage mImageLoader;

    private final String imageUrl = "https://images.xiaozhuanlan.com/photo/2020/4919e1e317209facf63c83d0686398bb.png";
    private final String imageUrlHeight = "https://images.xiaozhuanlan.com/photo/2020/b5a8f22bb93491d3b31ec2d60c709cf9.png";
    private final String patchImageUrl = "http://kongzue.com/test/img_notification_ios.9.png";
    private final String imageUrlTest = "https://images.xiaozhuanlan.com/photo/2020/de67120589fd1b314c7a2e75a2233b06.png";
    private final String gifUrl = "https://images.xiaozhuanlan.com/photo/2020/e09ab1c37ca4fe242e63bc6ef2f34551.gif";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);
        img4 = findViewById(R.id.img4);
        img5 = findViewById(R.id.img5);
        img6 = findViewById(R.id.img6);
        img7 = findViewById(R.id.img7);
        img10 = findViewById(R.id.img10);
        img11 = findViewById(R.id.img11);
        img12 = findViewById(R.id.img12);
        img13 = findViewById(R.id.img13);
        img14 = findViewById(R.id.img14);

        /**
         * 自定义ImageConfig使用方法 需要自定义写构造方法或者再次实现建造者模式
         */
        /*
        ImageConfig imageConfig = new ImageConfig();
        imageConfig.setVisibility(true);
        imageConfig.setUrl("");
        imageConfig.setImageView(img1);
        mImageLoader.loadImage(this, imageConfig);
         */
        mImageLoader = new SImage();

        //使用自定义ImageConfig的建造者模式加载
        mImageLoader.loadImage(this, ImageConfig.builder().url(Uri.parse(imageUrl)).imageView(img1).show());
        mImageLoader.loadImage(this, ImageConfig.builder().url(Uri.parse(imageUrl)).imageView(img2).isCircle(true).show());
        mImageLoader.loadImage(this, ImageConfig.builder().url(imageUrl).imageView(img3).setRadius(30).show());
        mImageLoader.loadImage(this, ImageConfig.builder().url(imageUrl).imageView(img4)
                .setTopRightRadius(10)
                .setTopLeftRadius(20)
                .setBottomRightRadius(30)
                .setBottomLeftRadius(0)
                .show());

        mImageLoader.loadImage(this, ImageConfig.builder().url(imageUrlHeight).imageView(img5).centerCrop(false).show());
        mImageLoader.loadImage(this, ImageConfig.builder().url(imageUrlHeight).imageView(img6).centerCrop(true).show());
        mImageLoader.loadImage(this, ImageConfig.builder().url(imageUrlHeight).imageView(img7).centerCrop(true)
                .setTopRightRadius(10)
                .setTopLeftRadius(20)
                .setBottomRightRadius(30)
                .setBottomLeftRadius(0)
//                .setRadius(30)
                .show());

        //使用BaseImageConfig的建造者模式加载
        mImageLoader.loadImage(this, BaseImageConfig.builder().url(R.mipmap.ic_launcher).imageView(img10).show());

        //使用SImage单例模式加载
        SImage.getInstance().loadImage(this, ImageConfig.builder().url(R.drawable.ic_baseline_adb_24).imageView(img11).show());

        //网络.9图片
        mImageLoader.loadImage(this, ImageConfig.builder().url(Uri.parse(patchImageUrl)).imageView(img12).show());
        mImageLoader.loadImage(this, ImageConfig.builder().url(patchImageUrl).imageView(img13).setListener(new OnLoadListener() {
            @Override
            public void onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                Log.e("OnResourceReady", "resource:" + resource.toString() + ",model:" + model.toString() + ",target:" + target.toString() + ",isFirstResource:" + isFirstResource);
            }

            @Override
            public void onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                Log.e("OnLoadFailed", "GlideException:" + e.toString() + ",model:" + model.toString() + ",target:" + target.toString() + ",isFirstResource:" + isFirstResource);
            }
        }).show());

        /**
         * 根据图片类型直出对象
         * 需要根据参数类型判断获取的字段,比如使用OnBitmapResult,就只有getBitmap方法不为null
         * 根据是否传入imageView是否直接显示图片,如果想自己处理过资源再加载则不传入imageView
         */
//        mImageLoader.loadImageAs(this, imageUrlTest, img14, new OnBitmapResult() {
//            @Override
//            public void OnResult(ImageResult result) {
//                Log.e("result", result.getBitmap() + "");
//            }
//        });
        /**
         * 使用File类型获取result时,默认result.getFile()是在设置的cache目录中
         */
//        mImageLoader.loadImageAs(this, imageUrlTest, img14, new OnFileResult() {
//            @Override
//            public void OnResult(ImageResult result) {
//                Log.e("result", result.getFile() + "");
//            }
//        });
//        mImageLoader.loadImageAs(this, gifUrl, img14, new OnGifResult() {
//            @Override
//            public void OnResult(ImageResult result) {
//                Log.e("result", result.getGif() + "");
//            }
//        });
        mImageLoader.loadImageAs(this, imageUrlTest, img14, new OnDrawableResult() {
            @Override
            public void OnResult(ImageResult result) {
                Log.e("result", result.getDrawable() + "");
            }
        });

        /**
         * 需求:
         * 1.网络.9图片 √
         * 2.加载失败重新加载 自定义次数
         * 3.加载前获取bitmap和宽高 √ (使用4自行获取宽高)
         * 4.加载完成后直出file √
         * 5.加载结果回调 √
         */

    }
}