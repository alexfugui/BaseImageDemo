package me.alex.aleximage.enmu;

/**
 * ================================================
 * Description:
 * <p>
 * Created by Alex on 2020/12/8 0008
 * <p>
 * 页面内容介绍:
 * <p>
 * ================================================
 */
public enum ImageType {
    FILE, BITMAP, GIF, DRAWABLE;
}
