package me.alex.aleximage;

/**
 * ================================================
 * Description:
 * <p>
 * Created by Alex on 2020/12/5
 * <p>
 * 页面内容介绍:
 * <p>
 * ================================================
 */
public class SImageConfig {
    public static SImageConfig sImageConfig = new SImageConfig();

    /**
     * 默认内存缓存大小 20mb
     */
    private int memoryCacheSize = 20;
    /**
     * 默认bitmap池缓存大小 30mb
     */
    private int bitmapPoolSize = 30;
    /**
     * 默认硬盘缓存大小250mb
     */
    private int diskCacheSize = 250;
    private String cacheFileName = "GlideAlexCache";


    public static SImageConfig getInstance() {
        return sImageConfig;
    }

    public int getMemoryCacheSize() {
        return memoryCacheSize;
    }

    /**
     * 设置内存缓存大小 默认20mb
     *
     * @param memoryCacheSize 单位mb
     * @return 返回SImageConfig类可以连续设置
     */
    public SImageConfig setMemoryCacheSize(int memoryCacheSize) {
        this.memoryCacheSize = memoryCacheSize;
        return this;
    }

    public int getBitmapPoolSize() {
        return bitmapPoolSize;
    }

    /**
     * 设置bitMap池缓存大小 默认30mb
     *
     * @param bitmapPoolSize 单位mb
     * @return 返回SImageConfig类可以连续设置
     */
    public SImageConfig setBitmapPoolSize(int bitmapPoolSize) {
        this.bitmapPoolSize = bitmapPoolSize;
        return this;
    }

    public int getDiskCacheSize() {
        return diskCacheSize;
    }

    /**
     * 设置硬盘缓存大小 默认250mb
     *
     * @param diskCacheSize 单位mb
     * @return 返回SImageConfig类可以连续设置
     */
    public SImageConfig setDiskCacheSize(int diskCacheSize) {
        this.diskCacheSize = diskCacheSize;
        return this;
    }

    public String getCacheFileName() {
        return cacheFileName;
    }

    public SImageConfig setCacheFileName(String cacheFileName) {
        this.cacheFileName = cacheFileName;
        return this;
    }
}
