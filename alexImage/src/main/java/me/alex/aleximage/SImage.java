package me.alex.aleximage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.ImageViewTarget;
import com.bumptech.glide.request.target.Target;

import java.io.File;

import me.alex.aleximage.entity.ImageResult;
import me.alex.aleximage.listener.OnAsListener;
import me.alex.aleximage.listener.OnBitmapResult;
import me.alex.aleximage.listener.OnDrawableResult;
import me.alex.aleximage.listener.OnFileResult;
import me.alex.aleximage.listener.OnGifResult;
import me.alex.aleximage.listener.OnLoadListener;

/**
 * ================================================
 * Description:
 * <p>
 * Created by Alex on 2020/12/4 0004
 * <p>
 * 页面内容介绍:
 * <p>
 * ================================================
 */
public final class SImage implements BaseImageLoaderStrategy<BaseImageConfig, OnAsListener> {

    public static SImage instance = new SImage();

    public static SImage getInstance() {
        return instance;
    }

    public SImage() {

    }

    @SuppressLint("CheckResult")
    @Override
    public void loadImage(@NonNull Context context, @NonNull BaseImageConfig config) {
        GlideRequests requests;
        requests = GlideAlex.with(context);//如果context是activity/Fragment则自动使用V层的生命周期
        GlideRequest<Drawable> glideRequest = requests.load(config.getUrl());

        final OnLoadListener listener = config.getListener();

        //占位图
        if (config.getPlaceholder() != -1) {
            glideRequest.placeholder(config.getPlaceholder());
        }
        //错误图
        if (config.getErrorPic() != -1) {
            glideRequest.error(config.getErrorPic());
        }
        //是否淡出淡入
        if (config.isCrossFade()) {
            glideRequest.transition(DrawableTransitionOptions.withCrossFade());
        }
        //是否将图片剪切为圆形
        if (config.isCircle()) {
            //如果剪裁成圆形就忽略圆角
            if (config.isCenterCrop()) {
                glideRequest.centerCrop();
            }
            glideRequest.circleCrop();
        } else {
            //圆形和圆角同时只能设置一种 同时设置只生效圆形
            if (config.getRadius() > 0) {
                if (config.isCenterCrop()) {//设置裁剪+4个圆角相同
                    glideRequest.transform(new RoundAndCenterCropTransform(
                            config.isCenterCrop(),
                            dp2px(context, config.getRadius()),
                            dp2px(context, config.getRadius()),
                            dp2px(context, config.getRadius()),
                            dp2px(context, config.getRadius()))
                    );
                } else {//单独设置4个圆角
                    glideRequest.transform(new RoundedCorners(config.getRadius()));
                }
            } else {
                //如果没设置通用圆角,设置了单独圆角+裁剪
                if (config.getTopRightRadius() > 0 || config.getTopLeftRadius() > 0 || config.getBottomRightRadius() > 0 || config.getBottomLeftRadius() > 0) {
                    glideRequest.transform(new RoundAndCenterCropTransform(
                            config.isCenterCrop(),
                            dp2px(context, config.getTopRightRadius()),
                            dp2px(context, config.getTopLeftRadius()),
                            dp2px(context, config.getBottomRightRadius()),
                            dp2px(context, config.getBottomLeftRadius()))
                    );
                } else {
                    //没有设置圆角
                    if (config.isCenterCrop()) {
                        //裁减
                        glideRequest.centerCrop();
                    }
                }
            }
        }

        if (listener != null) {
            glideRequest.listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    listener.onLoadFailed(e, model, target, isFirstResource);
//                    Log.e("onLoadFailed", "GlideException:" + e.toString() + ",model:" + model.toString() + ",target:" + target.toString() + ",isFirstResource:" + isFirstResource);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    listener.onResourceReady(resource, model, target, dataSource, isFirstResource);
//                    Log.e("onLoadFailed", "resource:" + resource.toString() + ",model:" + model.toString() + ",target:" + target.toString() + ",isFirstResource:" + isFirstResource);
                    return false;
                }
            });
        }

        glideRequest.into(config.getImageView());
    }

    @Override
    public void loadImageAs(@NonNull Context context, @NonNull Object url, @NonNull OnAsListener listener) {
        loadImageAs(context, url, null, listener);
    }

    @Override
    public void loadImageAs(@NonNull final Context context, @NonNull final Object url, @Nullable final ImageView imageView, @NonNull final OnAsListener listener) {
        GlideRequests requests;
        requests = GlideAlex.with(context);//如果context是activity/Fragment则自动使用V层的生命周期
        if (listener instanceof OnBitmapResult) {
            requests.asBitmap()
                    .load(url)
                    .into(new ImageViewTarget<Bitmap>(imageView != null ? imageView : new ImageView(context)) {
                        @Override
                        protected void setResource(@Nullable Bitmap resource) {
                            if (resource != null) {
                                if (imageView != null) {
                                    imageView.setImageBitmap(resource);
                                }
                                listener.OnResult(new ImageResult(resource));
                            }
                        }
                    });
        } else if (listener instanceof OnFileResult) {
            requests.asFile()
                    .load(url)
//                    .error(GlideAlex.with(context).asFile().load(url))
                    .listener(new RequestListener<File>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<File> target, boolean isFirstResource) {
                            if (imageView != null) {
                                new Handler().post(new Runnable() {
                                    @Override
                                    public void run() {
                                        GlideAlex.with(context).load(url).into(imageView);
                                    }
                                });
                            }
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(File resource, Object model, Target<File> target, DataSource dataSource, boolean isFirstResource) {
                            if (imageView != null) {
                                new Handler().post(new Runnable() {
                                    @Override
                                    public void run() {
                                        GlideAlex.with(context).load(url).into(imageView);
                                    }
                                });
                            }
                            return false;
                        }
                    })
                    .into(new ImageViewTarget<File>(imageView != null ? imageView : new ImageView(context)) {
                        @Override
                        protected void setResource(@Nullable File resource) {
                            if (resource != null) {
                                listener.OnResult(new ImageResult(resource));
                            }
                        }
                    });
        } else if (listener instanceof OnGifResult) {
            requests.asGif()
                    .load(url)
                    .into(new ImageViewTarget<GifDrawable>(imageView != null ? imageView : new ImageView(context)) {
                        @Override
                        protected void setResource(@Nullable GifDrawable resource) {
                            if (resource != null) {
                                if (imageView != null) {
                                    imageView.setImageDrawable(resource);
                                }
                                listener.OnResult(new ImageResult(resource));
                            }
                        }
                    });
        } else if (listener instanceof OnDrawableResult) {
            requests.asDrawable()
                    .load(url)
                    .into(new ImageViewTarget<Drawable>(imageView != null ? imageView : new ImageView(context)) {
                        @Override
                        protected void setResource(@Nullable Drawable resource) {
                            if (resource != null) {
                                if (imageView != null) {
                                    imageView.setImageDrawable(resource);
                                }
                                listener.OnResult(new ImageResult(resource));
                            }
                        }
                    });
        } else {
            requests.asBitmap()
                    .load(url)
                    .into(new ImageViewTarget<Bitmap>(imageView != null ? imageView : new ImageView(context)) {
                        @Override
                        protected void setResource(@Nullable Bitmap resource) {
                            if (resource != null) {
                                if (imageView != null) {
                                    imageView.setImageBitmap(resource);
                                }
                                listener.OnResult(new ImageResult(resource));
                            }
                        }
                    });
        }
//        GlideRequest<Drawable> glideRequest = requests.load(url);
    }


    @Override
    public void clear(@Nullable Context ctx, @Nullable BaseImageConfig config) {

    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    private int dp2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    private int px2dp(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }
}
